import React, { Component } from 'react';
import styled from 'styled-components';

const Box = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`

export default class DisplayPrefs extends Component {
  render() {
    return (
      <Box>
        {this.props.yourPref}
      </ Box>
    )

  }
}
