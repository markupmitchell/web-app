import React, { Component } from 'react';
import styled from 'styled-components';

const BigButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 200px;
  height: 100px;
  background: ${props => props.color};
`
// ^^ styling from props above doesn't work for some reason

export default class MusicPref extends Component {
  render() {
    return (
      <BigButton style={{background:this.props.color}} onClick={this.props.clickHandler.bind(this, this.props.value)}>
        {this.props.value}
      </ BigButton>
    )

  }
}
