import React, { Component } from 'react';
import './App.css';
import fire from './fire';
import * as firebase from 'firebase';
import MusicPref from './MusicPref';
import DisplayPrefs from './DisplayPrefs';
import OnlineUsers from './OnlineUsers';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      currentUser: 'null',
      preference: 'Either Way',
      usersObj: null
      },
    this.expressPref = this.expressPref.bind(this);
    this.authenticate = this.authenticate.bind(this);
  }

  componentWillMount(){

    let userRef = fire.database().ref('users'); /* HANG ON! will this update every time a new user's added?! */
      userRef.on('value', snapshot => { /* I need this to watch for authenticated user instead... */
      this.setState({ usersObj: [snapshot.val()]})
    });
  }

  authenticate() {
    const  provider = new firebase.auth.GoogleAuthProvider();
    const promise = firebase.auth().signInWithPopup(provider);

    promise.catch(e => console.log(e.message));
    promise.then(user => {
      const currentUser = firebase.auth().currentUser;
      firebase.database().ref('users/' + currentUser.uid).set({
        username: currentUser.displayName,
        preference: 'Either Way',
        email: currentUser.email
      });

      let preferenceRef = firebase.database().ref('users/' + currentUser.uid + '/preference');
      preferenceRef.on('value', snapshot => { /* setting an observer on the path defined above */
        this.setState({ preference: [snapshot.val()] }); /* ... which updates local state */
        });
      });
  }

  expressPref(pref) {
    /* Send the message to Firebase */
    const currentUser = firebase.auth().currentUser;
    const ref = fire.database().ref('users');
    ref.child(currentUser.uid).update({ preference: pref });

  }

  render() {
    if (!this.state.usersObj){
      return <div> loading </div>
    }
    return (
      <div>
        <div>{this.state.currentUser}</div>
        <MusicPref clickHandler={this.expressPref} value={'ON'} color={'red'} />
        <MusicPref clickHandler={this.expressPref} value={'OFF'} color={'grey'} />
        <MusicPref clickHandler={this.expressPref} value={'Either Way'} color={'green'} />
        <DisplayPrefs yourPref={this.state.preference} />
        <button onClick={this.authenticate}>AUTHENTICATE</button>
        <OnlineUsers onlineUsers={this.state.usersObj} />
      </div>
    )
    }
  }
