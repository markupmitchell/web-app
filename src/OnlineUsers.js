
import React from 'react';

function OnlineUsers(props) {
  const usersObj = props.onlineUsers[0];
  const IDs = Object.keys(usersObj);
  // debugger;
  console.log(Object.keys(usersObj));
  
  
  return (
    <ul>{
      IDs.map(ID => {
        return <li key={ID}>{usersObj[ID].email}</li>
        })
      }
    </ul>
  )
}

export default OnlineUsers;
